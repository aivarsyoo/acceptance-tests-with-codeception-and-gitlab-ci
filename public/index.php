<?php declare(strict_types=1);

/**
 * (!) CAUTION (!)
 * This whole script is quick and dirty and for the purpose of demonstration!
 * Don't write your applications like this!
 * You have been warned!
 */
$db = new class($_ENV['DB_DSN'], $_ENV['DB_USER'], $_ENV['DB_PASSWORD']) {
    private \PDO $pdo;

    public function __construct(string $dsn, string $user, string $pass)
    {
        $this->pdo = new \PDO($dsn, $user, $pass);
    }

    public function getTasks(): array
    {
        $stmt = $this->pdo->query('SELECT `task` FROM `todo`');

        return array_map(
                static function (array $row) {
                    $row['task'] = htmlentities($row['task']);
                    return $row;
                },
                $stmt->fetchAll(PDO::FETCH_ASSOC)
        );
    }

    public function addTask(array $postData): void
    {
        if (array_key_exists('task', $postData) && $postData['task']) {
            $stmt = $this->pdo->prepare('INSERT INTO `todo` (`task`) VALUES (:task)');
            $stmt->bindParam('task', $postData['task']);
            $stmt->execute();

            header('Location: /', true, 302);
            exit(0);
        }
    }
};

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $db->addTask($_POST);
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>“Acceptance Tests with Codeception and Gitlab-CI” or how to solve a hen-egg problem</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>
<body class="container">
<h1>Another TODO App</h1>
<h2>Tasks:</h2>
<ul>
    <?php foreach ($db->getTasks() as $task) : ?>
        <li><?= $task['task'] ?></li>
    <?php endforeach; ?>
</ul>
<form id="todo-form" method="post">
    <label for="task">Add task:</label>
    <input type="text" id="task" name="task" placeholder="My new task..." autocomplete="off"/>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
</body>
</html>
